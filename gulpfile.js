
var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('sass', function () {
    return gulp.src('./src/AppBundle/Resources/sass/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest('./web/src/css/'));
});

gulp.task('watch', function () {
    gulp.watch('./src/**/*.scss', ['sass']);
});