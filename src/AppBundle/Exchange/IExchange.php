<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 15.01.2018
 * Time: 21:34
 */

namespace AppBundle\Exchange;

interface IExchange
{
    public function getCurrencyByCityName($city, $amount, $from);
}