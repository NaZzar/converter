<?php

namespace AppBundle\Exchange;


class RestConverter{

    private $baseURL;
    private $data;

    public function __construct(){
        $this->baseURL = 'http://finance.google.com/finance/converter';
    }

    public function converting($amount, $moneyCodeFrom, $moneyCodeFromTo){
        $url = $this->baseURL.'?a='.$amount.'&from='.$moneyCodeFrom.'&to='.$moneyCodeFromTo;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $this->data = curl_exec($curl);
        curl_close($curl);

        return $this;
    }

    public function getResultConverting(){
        $result = 0;
        preg_match_all("/<span class=bld>(.*)<\/span>/", $this->data, $converted);
        if(!empty($converted[1])){
            $result = preg_replace("/[^0-9.]/", "", $converted[1][0]);
        }
        return $result;
    }

}