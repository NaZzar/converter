<?php

namespace AppBundle\Exchange;
use Unirest\Exception;

class RestCountries {

    private $baseURL;
    private $dataJSON;

    public function __construct(){
        $this->baseURL = 'https://restcountries.eu/rest/v2/capital/';
    }

    public function getDataCapitalCity($city){

        $url = $this->baseURL.$city;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $this->dataJSON = curl_exec($curl);
        curl_close($curl);

        return $this;
    }

    public function getCurrencyCode(){
        try{
            $capitalCityData = json_decode($this->dataJSON);
            if(isset($capitalCityData->status))
                throw new Exception('No city data!');
            $code = $capitalCityData[0]->currencies[0]->code;
        }catch (\Exception $e) {
            $code = false;
        }
        return $code;
    }

}