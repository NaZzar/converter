<?php

namespace AppBundle\Exchange;

class Currency implements IExchange {

    public function getCurrencyByCityName($nameCity, $amount, $moneyCodeFrom){

        $city = new RestCountries();
        $moneyCodeTo = $city->getDataCapitalCity($nameCity)->getCurrencyCode();

        if ($moneyCodeTo) {
            $converter = new RestConverter();
            $resultConverting = $converter->converting($amount, $moneyCodeFrom, $moneyCodeTo)->getResultConverting();
        } else {
            $resultConverting = 0;
        }

        return $resultConverting;
    }

}