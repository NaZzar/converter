<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Exchange
 *
 * @ORM\Table(name="exchange")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExchangeRepository")
 */
class Exchange
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=100)
     */
    private $city;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    private $date;

    /**
     * @var float
     *
     * @ORM\Column(name="cash_start", type="float")
     */
    private $cashStart;

    /**
     * @var float
     *
     * @ORM\Column(name="cash_exchange", type="float")
     */
    private $cashExchange;

    /**
     * @var string
     *
     * @ORM\Column(name="cash_start_code", type="string", length=10)
     */
    private $cashStartCode;

    /**
     * @var string
     *
     * @ORM\Column(name="cash_exchange_code", type="string", length=10)
     */
    private $cashExchangeCode;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Exchange
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Exchange
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set cashStart
     *
     * @param float $cashStart
     *
     * @return Exchange
     */
    public function setCashStart($cashStart)
    {
        $this->cashStart = $cashStart;

        return $this;
    }

    /**
     * Get cashStart
     *
     * @return float
     */
    public function getCashStart()
    {
        return $this->cashStart;
    }

    /**
     * Set cashExchange
     *
     * @param float $cashExchange
     *
     * @return Exchange
     */
    public function setCashExchange($cashExchange)
    {
        $this->cashExchange = $cashExchange;

        return $this;
    }

    /**
     * Get cashExchange
     *
     * @return float
     */
    public function getCashExchange()
    {
        return $this->cashExchange;
    }

    /**
     * Set cashStartCode
     *
     * @param string $cashStartCode
     *
     * @return Exchange
     */
    public function setCashStartCode($cashStartCode)
    {
        $this->cashStartCode = $cashStartCode;

        return $this;
    }

    /**
     * Get cashStartCode
     *
     * @return string
     */
    public function getCashStartCode()
    {
        return $this->cashStartCode;
    }

    /**
     * Set cashExchangeCode
     *
     * @param string $cashExchangeCode
     *
     * @return Exchange
     */
    public function setCashExchangeCode($cashExchangeCode)
    {
        $this->cashExchangeCode = $cashExchangeCode;

        return $this;
    }

    /**
     * Get cashExchangeCode
     *
     * @return string
     */
    public function getCashExchangeCode()
    {
        return $this->cashExchangeCode;
    }
}

