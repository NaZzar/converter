<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Exchange;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Exchange\Currency;
use AppBundle\Exchange\RestCountries;
use Unirest\Exception;

/**
 * Exchange controller.
 *
 */
class ExchangeController extends Controller{

    /**
     * index page exchange.
     *
     */
    public function indexAction(Request $request)
    {
        $moneyCodeFrom = 'PLN';
        $result =array();
        $error = '';

        $exchange = new Exchange();
        $form = $this->createForm('AppBundle\Form\ExchangeType', $exchange);
        $form->handleRequest($request);

        try {
            if ($form->isSubmitted()) {
                if($form->isValid()){

                    $currency = new Currency();
                    $resultExchange  = $currency->getCurrencyByCityName($exchange->getCity(), $exchange->getCashStart(),$moneyCodeFrom);

                    if($resultExchange!=0){

                        $city = new RestCountries();
                        $moneyCodeTo = $city->getDataCapitalCity($exchange->getCity())->getCurrencyCode();

                        $exchange->setCashExchange($resultExchange);
                        $exchange->setCashStartCode($moneyCodeFrom);
                        $exchange->setCashExchangeCode($moneyCodeTo);
                        $exchange->setDate(new \DateTime('NOW'));

                        $em = $this->getDoctrine()->getManager();
                        $em->persist($exchange);
                        $em->flush();

                        $result = array(
                            'amount_start'=>$exchange->getCashStart().' '.$moneyCodeFrom,
                            'amount_exchange'=>round($resultExchange, 2).' '.$moneyCodeTo
                        );
                    }else{
                        throw new Exception('Country with such capital was not found!');
                    }
                }else{
                    throw new Exception('The entered data is not correct!');
                }
            }

        }catch (\Exception $e) {
            $error = $e->getMessage();
        }

        return $this->render('exchange/index.html.twig', array(
            'error' => $error,
            'result' => $result,
            'form' => $form->createView(),
        ));
    }

    /**
     * show history.
     *
     */
    public function showAction(Request $request){

        $em = $this->getDoctrine()->getManager();
        $exchanges = $em->getRepository('AppBundle:Exchange')->findBy(array(), array('date' => 'DESC'));

        return $this->render('exchange/show.html.twig', array(
            'exchanges' => $exchanges
        ));
    }

}
