<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 18.01.2018
 * Time: 18:21
 */

namespace Tests\AppBundle\Exchange;
use PHPUnit\Framework\TestCase;

use AppBundle\Exchange\RestCountries;

class RestCountriesTest extends TestCase {

    public function testGetCodeCurencyByCapitalCityTrue(){
        $city = new RestCountries();
        $moneyCodeTo = $city->getDataCapitalCity('kiev')->getCurrencyCode();
        $this->assertEquals('UAH' , $moneyCodeTo);
    }

    public function testGetCodeCurencyByCapitalCityFalse(){
        $city = new RestCountries();
        $moneyCodeTo = $city->getDataCapitalCity('kievv')->getCurrencyCode();
        $this->assertEquals(false , $moneyCodeTo);
    }
}
