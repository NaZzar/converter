<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 18.01.2018
 * Time: 18:30
 */

namespace Tests\AppBundle\Exchange;
use PHPUnit\Framework\TestCase;
use AppBundle\Exchange\Currency;

class CurrencyTest extends TestCase{

    public function testGetCurrencyByCityNameTrue(){
        $currency = new Currency();
        $resultExchange  = $currency->getCurrencyByCityName('kiev', '1','PLN');
        $this->assertEquals(8 , round($resultExchange, 0));
    }

    public function testGetCurrencyByCityNameFalse(){
        $currency = new Currency();
        $resultExchange  = $currency->getCurrencyByCityName('kievv', '1','PLN');
        $this->assertEquals(0 , $resultExchange);
    }

}
