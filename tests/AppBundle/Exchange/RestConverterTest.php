<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 18.01.2018
 * Time: 01:36
 */

namespace Tests\AppBundle\Exchange;

use AppBundle\Exchange\RestConverter;
use PHPUnit\Framework\TestCase;

class RestConverterTest extends TestCase{

    public function testConvertingTrue(){
        $converter = new RestConverter();
        $resultConverting = $converter->converting(1, 'PLN', 'UAH')->getResultConverting();

        $this->assertEquals(8, round($resultConverting, 0));
    }

    public function testConvertingFalse(){
        $converter = new RestConverter();
        $resultConverting = $converter->converting(-1, 'PLNN', 'UAHH')->getResultConverting();
        $this->assertEquals(0 , $resultConverting);
    }


}
