<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase{

    public function testIndexPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Currency Converter', $crawler->filter('h1')->text());
    }

    public function testHistoryPage(){
        $client = static::createClient();
        $crawler = $client->request('GET', '/show_history');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Currency Converter', $crawler->filter('h1')->text());
    }

    public function testNotIssetPage(){
        $client = static::createClient();
        $client->request('GET', '/notIsset');
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }
}
